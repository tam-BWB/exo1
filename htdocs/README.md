/*Exercice HTML+CSS

    - FAIT
        - Tous les objets de la maquette sont dans le rendu, aussi la table, ceux qui sont des boutons et les icones
        - Les objets qui devaient avoir des liens à des pages web, a des pdf, à l'envoie d'un courrier et au formulaire, sont tous fait
        - Pour l'animation j'ai fait un changement de couleur des boutons de la barre d'en haut lorsque'on les survoles
        - La table, même s'il y a des différences, le changement couleur selon la ligne, les bords et l'alignement sont respectés


    - PAS FAIT
        - Partie responsive, car j'ai préféré passer à la partie bootstrap aujourd'hui pour voir ce que ça donnait (voir des tutos, lire la doc et faire des essais et commencer à refaire la maquette), néanmoins lorsque je finirai la partie bootstrap je retournerai vers la partie responsive de mon html+css
        - Faire un formulaire propre dans la partie Déconnexion et aussi que la partie Inscription renvoie vers un autre formulaire
        - Utiliser un autre type d'icone qui est relié à fontawesome
        - Utiliser les nommages autres que DIV, j'ai pas fait attention à cela


    - PAS REUSSIT
        - Bien cadrer toutes les div sans l'aide de MARGIN, au final j'ai du utiliser MARGIN pour celle du Contact PRO BTS
        - Pas toutes les parties ou div sont bien cadrés


/*Exercice Bootstrap (en cours de réalisation)